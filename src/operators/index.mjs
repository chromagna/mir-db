import { $in } from "./boolean/in.mjs";
import { $includesAll } from "./boolean/includesAll.mjs";
import { $includesAny } from "./boolean/includesAny.mjs";
import { $nin } from "./boolean/nin.mjs";
import { $excludesAll } from "./boolean/excludesAll.mjs";
import { $excludesAny } from "./boolean/excludesAny.mjs";
import { $excludes } from "./boolean/excludes.mjs";
import { $like } from "./boolean/like.mjs";
import { $lt } from "./boolean/lt.mjs";
import { $lte } from "./boolean/lte.mjs";
import { $gt } from "./boolean/gt.mjs";
import { $gte } from "./boolean/gte.mjs";
import { $eqeq, $eqeqeq, $neq, $neqeq } from "./boolean/equals.mjs";
import { $fn } from "./boolean/fn.mjs";
import { $or } from "./boolean/or.mjs";
import { $and } from "./boolean/and.mjs";

import { $set } from "./mutation/set.mjs";
import { $change } from "./mutation/change.mjs";
import { $unset } from "./mutation/unset.mjs";
import { $inc, $dec, $mult, $div } from "./mutation/math.mjs";
import { $append } from "./mutation/append.mjs";
import { $unshift, $push } from "./mutation/concat.mjs";
import { $pull } from "./mutation/pull.mjs";
import { $insertBefore } from "./mutation/insertBefore.mjs";
import { $insertAfter } from "./mutation/insertAfter.mjs";
import { Ok } from "../utils.mjs";

export let operators = {
	$in,
	$includesAll,
	$includesAny,
	$nin,
    $excludesAll,
    $excludesAny,
    $excludes,
    $like,
    $lt,
    $lte,
    $gt,
    $gte,
    $eqeq,
    $eqeqeq,
    $neq,
    $neqeq,
    $fn,
    $or,
    $and,

    $set,
    $change,
    $append,
    $unset,
    $inc, $dec, $mult, $div,
    $unshift, $push,
    $pull,
    $insertBefore,
    $insertAfter,
};

export const processMutationOperators = async (sourceItems, objWithOperators, query, db) => {
    Ok(objWithOperators).forEach(async (mod) => {
        // TODO : replace with safeHasOwnProperty
        if (!operators[mod]) {
            console.warn(`Modifier [${mod}] is unknown.`);
            return;
        }

        sourceItems = await operators[mod](sourceItems, objWithOperators[mod], query, db);
    });

    return sourceItems;
};
