import { isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

const equals = (sourceItem, predicate, key) => {
	let match = false;

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let query = predicate[pk][key] || "";

			if (key == "$eqeq") {
				if (safeHasOwnProperty(sourceItem, pk) && sourceItem[pk] == query) match = true;
			} else if (key == "$eqeqeq") {
				if (safeHasOwnProperty(sourceItem, pk) && sourceItem[pk] === query) match = true;
			}
		});
	}

	return match;
};

const notEquals = (sourceItem, predicate, key) => {
	let match = false;

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let query = predicate[pk][key] || "";

			if (key == "$neq") {
				if (Object.prototype.hasOwnProperty.call(sourceItem, pk) && sourceItem[pk] != query) match = true;
			} else if (key == "$neqeq") {
				if (Object.prototype.hasOwnProperty.call(sourceItem, pk) && sourceItem[pk] !== query) match = true;
			}
		});
	}

	return match;
};

export const $eqeq = (sourceItem, predicate) => equals(sourceItem, predicate, "$eqeq");
export const $eqeqeq = (sourceItem, predicate) => equals(sourceItem, predicate, "$eqeqeq");
export const $neq = (sourceItem, predicate) => notEquals(sourceItem, predicate, "$neq");
export const $neqeq = (sourceItem, predicate) => notEquals(sourceItem, predicate, "$neqeq");
