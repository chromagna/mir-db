import { ensureArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $lt = (sourceItem, predicate) => {
	let match = true;

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let query = predicate[pk]["$lt"];
			query = ensureArray(query);

			if (safeHasOwnProperty(sourceItem, pk)) {
                for (const q of query) if (sourceItem[pk] >= q) match = false;
			}
		});
	}

	return match;
};
