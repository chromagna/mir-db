import { isObject, Ok, ensureArray, safeHasOwnProperty } from "../../utils.mjs";

export const $fn = (sourceItem, predicate) => {
    let match = true;

    if (isObject(predicate)) {
        Ok(predicate).forEach((pk) => {
            if (safeHasOwnProperty(sourceItem, pk)) {
                let fns = predicate[pk]["$fn"];
                fns = ensureArray(fns);

                for (const fn of fns) {
                    if (!fn(sourceItem[pk])) match = false;
                }
            }
        });
    }

    return match;
};
