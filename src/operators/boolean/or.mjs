import { checkAgainstPredicate } from "../../helpers.mjs";
import { ensureArray, isFunction, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $or = (sourceItem, predicate) => {
	let matches = [];

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			if (pk === "$or") {
				let ors = predicate[pk];
				ors = ensureArray(ors);
				for (const or of ors) {
					matches.push(checkAgainstPredicate(sourceItem, or));
				}
			}

			if (safeHasOwnProperty(sourceItem, pk)) {
				let ors = predicate[pk];
				ors = ensureArray(ors);

				for (const or of ors) {
					if (isObject(or)) {
						for (const orProp of Ok(or)) {
							if (orProp == pk) {
								if (isFunction(or[orProp])) {
									matches.push(or[orProp](sourceItem[pk]));
								} else {
									matches.push(checkAgainstPredicate(sourceItem, or));
								}
							}
						}
					}

					if (isFunction(or)) {
						matches.push(or(sourceItem[pk]));
					}
				}
			}
		});
	}

	if (!matches.length) return false;
	if (matches.length && matches.includes(true)) return true;
	return false;
};
