import { ensureArray, isArray, isObject, Ok } from "../../utils.mjs";

export const $includesAny = (sourceItem, predicate) => {
	let matches = [];
	let hadKeyMatch = false;

	if (isObject(predicate)) {
		Ok(predicate).forEach((predKey) => {
			let valuesToMatch = predicate[predKey]["$includesAny"] || [];
			valuesToMatch = ensureArray(valuesToMatch);
			if (Ok(sourceItem, predKey) && isArray(sourceItem[predKey])) {
				hadKeyMatch = true;
				for (const v of valuesToMatch) matches.push(sourceItem[predKey].includes(v));
			}
		});
	}

	if (!hadKeyMatch) return false;
	if (!matches.length) return false;
	return matches.includes(true);
};
