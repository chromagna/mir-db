import { isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $nin = (sourceItem, predicate) => {
	let match = true;
	if (isObject(predicate)) {
		Ok(predicate).forEach((predKey) => {
			let queryArray = predicate[predKey]["$nin"] || [];
			if (safeHasOwnProperty(sourceItem, predKey) && queryArray.includes(sourceItem[predKey])) {
				match = false;
			}
		});
	}
	return match;
};
