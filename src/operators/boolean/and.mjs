import { checkAgainstPredicate } from "../../helpers.mjs";
import { ensureArray, isFunction, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $and = (sourceItem, predicate) => {
    let matches = [];

    if (isObject(predicate)) {
        Ok(predicate).forEach((pk) => {
            if (pk === "$and") {
                let ands = predicate[pk];
                ands = ensureArray(ands);
                for (const and of ands) {
                    Ok(and).forEach((andKey) => {
                        // hmm...
                        if (andKey.startsWith("$")) {
                            matches.push(checkAgainstPredicate(sourceItem, and));
                        } else if (safeHasOwnProperty(sourceItem, andKey)) {
                            matches.push(checkAgainstPredicate(sourceItem, and));
                        }
                    })
                }
            }

            if (safeHasOwnProperty(sourceItem, pk)) {
                let ands = predicate[pk]["$and"];
                ands = ensureArray(ands);
                for (const and of ands) {
                    if (isObject(and)) {
                        for (const andProp of Ok(and)) {
                            if (andProp == pk) {
                                if (isFunction(and[andProp])) {
                                    matches.push(and[andProp](sourceItem[pk]));
                                } else {
                                    matches.push(checkAgainstPredicate(sourceItem, and));
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    if (!matches.length) return false;
    if (matches.length && matches.includes(false)) return false;
    return true;
};
