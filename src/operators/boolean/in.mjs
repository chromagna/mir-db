import { isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $in = (sourceItem, predicate) => {
	let match = true;
	if (isObject(predicate)) {
		Ok(predicate).forEach((predKey) => {
			let inArray = predicate[predKey]["$in"] || [];
			if (safeHasOwnProperty(sourceItem, predKey) && !inArray.includes(sourceItem[predKey])) {
				match = false;
			}
		});
	}
	return match;
};
