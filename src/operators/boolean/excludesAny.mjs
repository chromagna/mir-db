import { ensureArray, isArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $excludesAny = (sourceItem, predicate) => {
	let matches = [];
	let hadKeyMatch = false;

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let valuesToMatch = predicate[pk]["$excludesAny"] || [];
			valuesToMatch = ensureArray(valuesToMatch);

			if (safeHasOwnProperty(sourceItem, pk) && isArray(sourceItem[pk])) {
				hadKeyMatch = true;
				for (const v of valuesToMatch) matches.push(sourceItem[pk].includes(v));
			}
		});
	}

	if (!hadKeyMatch) return false;
	if (!matches.length) return false;
	if (matches.includes(false)) return true;
	return false;
};
