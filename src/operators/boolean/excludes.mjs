import { ensureArray, isArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $excludes = (sourceItem, predicate) => {
    let matches = [];

    if (isObject(predicate)) {
        Ok(predicate).forEach((pk) => {
            let valuesToFind = predicate[pk]["$excludes"] || [];
            valuesToFind = ensureArray(valuesToFind);

            if (safeHasOwnProperty(sourceItem, pk) && isArray(sourceItem[pk])) {
                for (const v of valuesToFind) matches.push(sourceItem[pk].includes(v));
            }
        });
    }

    if (!matches.length) return false;
    if (matches.length && matches.includes(true)) return false;
    return true;
};
