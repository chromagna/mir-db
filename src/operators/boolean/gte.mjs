import { ensureArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $gte = (sourceItem, predicate) => {
	let match = true;

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let query = predicate[pk]["$gte"];
			query = ensureArray(query);

			if (safeHasOwnProperty(sourceItem, pk)) {
				for (const q of query) if (sourceItem[pk] < q) match = false;
			}
		});
	}

	return match;
};
