import { ensureArray, isObject, isString, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $like = (sourceItem, predicate) => {
	let matches = [];

	if (isObject(predicate)) {
		Ok(predicate).forEach((pk) => {
			let query = predicate[pk]["$like"];
			query = ensureArray(query);

			if (safeHasOwnProperty(sourceItem, pk) && isString(sourceItem[pk])) {
				for (const q of query) matches.push(sourceItem[pk].includes(q));
			}
		});
	}

    return matches.includes(true);
};
