import { isArray } from "../../utils.mjs";
import { ensureArray } from "../../utils.mjs";
import { isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $excludesAll = (sourceItem, predicate) => {
    let matches = [];
    let hadKeyMatch = false;

    if (isObject(predicate)) {
        Ok(predicate).forEach((pk) => {
            let valuesToMatch = predicate[pk]["$excludesAll"] || [];
            valuesToMatch = ensureArray(valuesToMatch);

            if (safeHasOwnProperty(sourceItem, pk) && isArray(sourceItem[pk])) {
                hadKeyMatch = true;
                for (const v of valuesToMatch) {
                    matches.push(sourceItem[pk].includes(v));
                }
            }
        });
    }

    if (!hadKeyMatch) return false;
    if (!matches.length) return false;
    if (matches.includes(true)) return false;
    return true;
};
