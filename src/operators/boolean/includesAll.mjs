import { ensureArray, isArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $includesAll = (sourceItem, predicate) => {
	let matches = [];
	let hadKeyMatch = false;

	if (isObject(predicate)) {
		Ok(predicate).forEach((predKey) => {
			let valuesToMatch = predicate[predKey]["$includesAll"] || [];
			valuesToMatch = ensureArray(valuesToMatch);

			if (safeHasOwnProperty(sourceItem, predKey) && isArray(sourceItem[predKey])) {
				hadKeyMatch = true;
				for (const v of valuesToMatch) matches.push(sourceItem[predKey].includes(v));
			}
		});
	}

	if (!hadKeyMatch) return false;
	if (!matches.length) return false;
	if (matches.includes(false)) return false;
	return true;
};
