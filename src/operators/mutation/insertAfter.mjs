import { idk } from "../../index.mjs";
import { insertObjectAfter } from "../../insertObject.mjs";
import { returnFound } from "../../returnFound.mjs";
import { ensureArray } from "../../utils.mjs";

export const $insertAfter = async (sourceItems, modifiers, query, db) => {
	modifiers = ensureArray(modifiers);

	for (const mod of modifiers) {
		sourceItems = insertObjectAfter(sourceItems, query, mod);
	}

	for (const item of sourceItems) {
		if (returnFound(item, query, idk)) {
			if (db.options.timestamps) item["updatedAt"] = Date.now();
		}
		await db.mergeShallow(item[idk], item);
	}

	return sourceItems;
};
