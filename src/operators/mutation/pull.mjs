import { returnFound } from "../../returnFound.mjs";
import { isArray } from "../../utils.mjs";
import { Ok } from "../../utils.mjs";
import { isObject } from "../../utils.mjs";
import { ensureArray } from "../../utils.mjs";
import _ from "lodash";

export const $pull = async (sourceItems, modifiers, query, db) => {
	modifiers = ensureArray(modifiers);

	for (const mod of modifiers) {
		for (const sourceItem of sourceItems) {
			if (isObject(mod)) {
				for (const mk of Ok(mod)) {
					let items = returnFound(sourceItem, query, mk);
					items = ensureArray(items);
					for (let item of items) {
						item = _.mergeWith(item, mod, function (objValue, srcValue) {
							srcValue = ensureArray(srcValue);

							if (isArray(objValue)) {
								return _.pull(objValue, ...srcValue);
							}

							return objValue;
						});
					}
				}
			}
		}
	}

	return sourceItems;
};
