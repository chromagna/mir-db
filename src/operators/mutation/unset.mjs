import { idk } from "../../index.mjs";
import { returnFound } from "../../returnFound.mjs";
import { ensureArray, isObject, Ok, safeHasOwnProperty } from "../../utils.mjs";

export const $unset = async (sourceItems, modifiers, query, db) => {
	modifiers = ensureArray(modifiers);

	for (const mod of modifiers) {
		if (isObject(mod)) {
			// { $unset: { a: 3 } }
			for (const sourceItem of sourceItems) {
				for (const mk of Ok(mod)) {
					let found = returnFound(sourceItem, query, mk);
					found = ensureArray(found);
					for (const f of found) {
						if (isObject(f) && safeHasOwnProperty(f, mk)) {
							if (f[mk] == mod[mk]) {
								delete f[mk];
							}
						}
					}
				}
			}
		} else {
			// { $unset: "a" }
			for (const sourceItem of sourceItems) {
				let found = returnFound(sourceItem, query, mod);
				found = ensureArray(found);
				for (const f of found) {
					if (isObject(f) && safeHasOwnProperty(f, mod)) {
						delete f[mod];
					}
				}
			}
		}
	}

	for (const item of sourceItems) {
		await db.mergeShallow(item[idk], item);
	}

	return sourceItems;
};
