import { changeProps } from "../../changeProps.mjs";
import { idk } from "../../index.mjs";
import { ensureArray, isObject, Ok } from "../../utils.mjs";

export const $change = async (sourceItems, modifiers, query, db) => {
	modifiers = ensureArray(modifiers);

	for (const mod of modifiers) {
		if (!isObject(mod) && !isArray(mod)) {
			let obj = {};

			for (const key of Ok(query)) {
				obj[key] = mod;
			}

			sourceItems = changeProps(sourceItems, query, obj);
		} else {
			sourceItems = changeProps(sourceItems, query, mod);
		}
	}

	for (const item of sourceItems) {
		await db.mergeShallow(item[idk], item);
	}

	return sourceItems;
};
