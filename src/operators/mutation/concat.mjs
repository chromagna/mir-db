import _ from "lodash";
import { idk } from "../../index.mjs";
import { returnFound } from "../../returnFound.mjs";
import { ensureArray, Ok } from "../../utils.mjs";

const concat = async (sourceItems, modifiers, query, db, unshift = false) => {
	modifiers = ensureArray(modifiers);

	for (const mod of modifiers) {
		for (const sourceItem of sourceItems) {
			for (const mk of Ok(mod)) {
				let items = returnFound(sourceItem, query, mk);
				items = ensureArray(items);
				for (let item of items) {
					item = _.mergeWith(item, mod, function (objValue, srcValue) {
						srcValue = ensureArray(srcValue);
						objValue = ensureArray(objValue);
						return unshift ? srcValue.concat(objValue) : objValue.concat(srcValue);
					});
				}
			}
		}
	}

	for (const item of sourceItems) {
		if (db.options.timestamps) item["updatedAt"] = Date.now();
		await db.mergeShallow(item[idk], item);
	}

	return sourceItems;
};

export const $unshift = async (sourceItems, modifiers, query, db) => {
	return concat(sourceItems, modifiers, query, db, true);
};

export const $push = async (sourceItems, modifiers, query, db) => {
	return concat(sourceItems, modifiers, query, db, false);
};
