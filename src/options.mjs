import _ from "lodash";
import { idk } from "./index.mjs";
import { Ov } from "./utils.mjs";
import { Ok } from "./utils.mjs";

const getSortFunctions = (keys) => {
    let fns = [];
    for (const key of keys) fns.push(item => item[key]);
    return fns;
};

const getSortDirections = (nums) => {
    let dirs = [];
    for (const num of nums) dirs.push(num === 1 ? "asc" : "desc");
    return dirs;
};

export const getDefaultDBOptions = (options = {}) => ({
    autosync: options.autosync ?? true,
    timestamps: options.timestamps ?? false,
    integerIds: options.integerIds ?? false,
    log: options.log ?? false,
});

export const getDefaultQueryOptions = (options) => ({
    deep: options.deep ?? true,
    returnKey: options.returnKey ?? idk,
    clonedData: options.clonedData ?? true,
    sort: options.sort ?? undefined,
    skip: options.skip ?? undefined,
    project: options.project ?? undefined,
});

export const applyQueryOptions = (data, options) => {
    if (options.sort) {
        data = _.orderBy(
            data,
            getSortFunctions(Ok(options.sort)),
            getSortDirections(Ov(options.sort)),
        );
    }

    if (options.project) {
        data = data.map(o => project(o, options.project));
    }

    return data;
};

const project = (o, p) =>
    Object.keys(p).reduce((r, k) => {
        if (!p[k]) return r;
        if (o[k]) r[k] = o[k]
        return r;
    }, {});