export const isArray = Array.isArray;
export const Ok = Object.keys;
export const Ov = Object.values;
export const Oe = Object.entries;
export const isFunction = (item) => typeof item === "function";
export const isString = (item) => typeof item === "string";
export const isObject = (item) => !!item && Object.prototype.toString.call(item) === "[object Object]";
export const isEmptyObject = (item) => isObject(item) && Ok(item).length === 0;
export const isEmptyArray = (item) => isArray(item) && item.length === 0;
export const isUndefined = (item) => typeof item === "undefined";
export const isNull = (item) => item === null;
export const safeHasOwnProperty = (obj, prop) => (obj ? Object.prototype.hasOwnProperty.call(obj, prop) : false);
export const ensureArray = (item) => {
    if (isArray(item)) return item;
    else if (isUndefined(item) || isNull(item)) return [];
    else return [item];
};
export const isNodeEnvironment = () => typeof process !== "undefined" && process.versions != null && process.versions.node != null;
export const isBrowserEnvironment = () => typeof window !== "undefined" && typeof window.document !== "undefined";
