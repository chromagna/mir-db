import { checkAgainstPredicate } from "./helpers.mjs";
import { isUndefined, Ok, isArray, isEmptyObject, isObject } from "./utils.mjs";

export const appendProps = (source, predicate, newProps) => {
    if (isUndefined(source)) return undefined;

    const processObject = (item) => {
        if (!isObject(item)) return item;

        let itemClone = { ...item };

        if (checkAgainstPredicate(itemClone, predicate)) itemClone = { ...itemClone, ...newProps };

        Ok(itemClone).forEach((key) => {
            if (isObject(itemClone[key]) || isArray(itemClone[key])) {
                itemClone = {
                    ...itemClone,
                    [key]: appendProps(itemClone[key], predicate, newProps),
                };
            }
        });

        return itemClone;
    };

    if ((isArray(source) || isObject(source)) && !isEmptyObject(predicate) && !isEmptyObject(newProps)) {
        return !isArray(source) ? processObject(source) : source.map(item => processObject(item));
    }

    return source;
};
