import { checkAgainstPredicate } from "./helpers.mjs";
import { Ok } from "./utils.mjs";
import { ensureArray, isArray, isEmptyObject, isObject, isUndefined, safeHasOwnProperty } from "./utils.mjs";

export const returnFound = (source, predicate, returnKey, parentDocument, lastNodeKey, deep = true) => {
    if (isUndefined(source)) return undefined;

    let result = undefined;

    const appendResult = (item) => {
        if (!item || isEmptyObject(item)) return;

        if (!result) result = [];
        result = ensureArray(result);

        // Ensure unique on returnKey
        if (isObject(result)) {
            if (result[returnKey] === item[returnKey]) return;
        }

        if (isArray(result)) {
            let found = false;
            for (const res of result) if (res[returnKey] === item[returnKey]) found = true;
            if (found) return;
        }

        result = result.concat(item);
    };

    const processObject = (item) => {
        if (!item) return;

        if (safeHasOwnProperty(item, returnKey)) parentDocument = item;
        if (checkAgainstPredicate(item, predicate)) appendResult(parentDocument);

        if (deep) {
            Ok(item).forEach((key) => {
                if (isObject(item[key]) || isArray(item[key])) {
                    appendResult(returnFound(item[key], predicate, returnKey, parentDocument, key, deep));
                }
            });
        }
    };

    source = ensureArray(source);

    if (isObject(predicate)) {
        source.forEach((stringOrObject, index) => {
            Ok(predicate).forEach((key) => {
                if (isObject(stringOrObject)) {
                    if (checkAgainstPredicate(source[index], predicate[key], lastNodeKey, key)) {
                        appendResult(parentDocument);
                    }
                } else if (checkAgainstPredicate(source, predicate[key], lastNodeKey, key)) {
                    appendResult(parentDocument);
                }
            });
        });
    }

    if (!isEmptyObject(predicate)) {
        source.map(item => processObject(item));
    } else {
        return source;
    }

    return result;
};
