import { isArray, isEmptyObject, isObject, isUndefined } from "./utils.mjs";

export const replaceObject = (source, predicate, replaceWith) => {
    if (isUndefined(source)) return undefined;

    const processObject = (item) => {
        if (!isObject(item)) return item;

        let itemClone = { ...item };

        if (checkAgainstPredicate(itemClone, predicate)) itemClone = { ...replaceWith };

        Ok(itemClone).forEach((key) => {
            if (isObject(itemClone[key]) || isArray(itemClone[key])) {
                itemClone = {
                    ...itemClone,
                    [key]: replaceObject(itemClone[key], predicate, replaceWith),
                };
            }
        });

        return itemClone;
    };

    if ((isArray(source) || isObject(source)) && !isEmptyObject(predicate) && !isEmptyObject(replaceWith)) {
        return !isArray(source) ? processObject(source) : source.map(item => processObject(item));
    }

    return source;
};
