import _ from "lodash";
import { idk } from "./index.mjs";
import { makeDistinctByKey } from "./index.mjs";
import { getDefaultDBOptions } from "./options.mjs";
import { applyQueryOptions } from "./options.mjs";
import { getDefaultQueryOptions } from "./options.mjs";
import { returnFound } from "./returnFound.mjs";
import { isEmptyObject, ensureArray, isUndefined, Ov, Ok } from "./utils.mjs";

const getDefaultJSONFormatOptions = (options = {}) => ({
    insideObjectBraces: options.insideObjectBraces ?? false,
    insideArrayBrackets: options.insideArrayBrackets ?? false,
    betweenPropsOrItems: options.betweenPropsOrItems ?? true,
    betweenPropNameAndValue: options.betweenPropNameAndValue ?? true,
});

const stringifyWithSpaces = (obj, options) => {
    options = getDefaultJSONFormatOptions(options);

    let result = JSON.stringify(obj, null, 1); // stringify, with line-breaks and indents
    result = result.replace(/^ +/gm, " "); // remove all but the first space for each line
    result = result.replace(/\n/g, ""); // remove line-breaks
    if (!options.insideObjectBraces) result = result.replace(/{ /g, "{").replace(/ }/g, "}");
    if (!options.insideArrayBrackets) result = result.replace(/\[ /g, "[").replace(/ \]/g, "]");
    if (!options.betweenPropsOrItems) result = result.replace(/, /g, ",");
    if (!options.betweenPropNameAndValue) result = result.replace(/": /g, `":`);
    return result;
};

export const find = (data = {}, query = [], options = {}, _options = {}, db = undefined) => {
    options = getDefaultQueryOptions(options);
    query = ensureArray(query);

    if (db?.options?.log) {
        if (JSON.stringify(options) !== JSON.stringify(getDefaultQueryOptions({}))) {
            console.log("FIND OPTIONS:", stringifyWithSpaces(options));
        }
        console.log("FIND QUERY:", stringifyWithSpaces(query));
    }

    // [ {} ] is problematic
    for (let i = 0; i < query.length; i++) {
        if (isEmptyObject(query[i])) {
            query.splice(i, 1);
        }
    }

    if (!query.length) {
        if (options.clonedData) {
            let distinctCloned = [];
            for (const obj of [ ...Ov(data) ]) distinctCloned.push(_.cloneDeep(obj));
            distinctCloned = applyQueryOptions(distinctCloned, options);
            if (db?.options?.log) {
                console.log("FIND RESULTS:");
                console.log(JSON.stringify(distinctCloned, null, 4));
            }
            return distinctCloned;
        } else {
            let d = [ ...Ov(data) ];
            let mutated = applyQueryOptions(d, options);
            if (db?.options?.log) {
                console.log("FIND RESULTS:");
                console.log(JSON.stringify(mutated, null, 4));
            }
            return mutated;
        }
    }

    let res = [];
    for (const q of query) {
        let r = [];
        if (q[idk]) {
            r.push(data[q[idk]]);
        } else if (q["id"] && _options.integerIds) {
            let f = data["__private"]["__id_map"][q["id"]];
            if (f) r.push(data[f["uuid"]]);
        } else {
            const fallbackFind = () => {
                r = returnFound(data, q, options.returnKey, null, "", options.deep);
                if (isUndefined(r)) r = [];
                r = ensureArray(r);
            };

            if (q["id"] && !_options.integerIds) {
                let f = data["__private"]["__id_map"][q["id"]];
                if (f && data[f["uuid"]]) r.push(data[f["uuid"]]);
                else fallbackFind();
            } else {
                fallbackFind();
            }
        }

        res.push(...r);
    }

    res = applyQueryOptions(res, options);

    if (!options.clonedData) {
        if (db?.options?.log) { console.log("Requested original, un-cloned data"); }
        let distinct = makeDistinctByKey(res, idk);
        if (db?.options?.log) {
            console.log("FIND RESULTS (original data):");
            console.log(JSON.stringify(distinct, null, 4));
        }
        return distinct;
    } else {
        let distinct = makeDistinctByKey(res, idk);
        let distinctCloned = [];
        for (const obj of distinct) distinctCloned.push(_.cloneDeep(obj));
        if (db?.options?.log) {
            console.log("FIND RESULTS (cloned data):");
            console.log(JSON.stringify(distinctCloned, null, 4));
        }
        return distinctCloned;
    }
};
