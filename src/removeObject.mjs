import { isArray, isEmptyObject, isObject } from "./utils.mjs";

export const removeObject = (source, predicate) => {
    if (isUndefined(source)) return undefined;

    const processObject = (item) => {
        if (!isObject(item)) return item;

        let itemClone = { ...item };

        Ok(itemClone).forEach((key) => {
            if (isObject(itemClone[key]) || isArray(itemClone[key])) {
                itemClone = {
                    ...itemClone,
                    [key]: removeObject(itemClone[key], predicate),
                };
            }
        });

        return itemClone;
    };

    if ((isArray(source) || isObject(source)) && !isEmptyObject(predicate)) {
        if (!isArray(source)) {
            if (!checkAgainstPredicate(source, predicate)) {
                return processObject(source);
            }
        } else {
            return source.filter(item => !checkAgainstPredicate(item, predicate))
                         .map(item => processObject(item));
        }
    } else {
        return source;
    }
};
