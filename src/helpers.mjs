import { operators } from "./operators/index.mjs";
import { isEmptyObject } from "./utils.mjs";
import { Ok } from "./utils.mjs";
import { isArray, isObject, safeHasOwnProperty } from "./utils.mjs";

export const checkAgainstPredicate = (sourceItem, predicate) => {
	if (typeof sourceItem !== typeof predicate) return false;

	if (isArray(sourceItem) && isArray(predicate)) return sourceItem.every((_, key) => checkAgainstPredicate(sourceItem[key], predicate[key]));

	if (isObject(sourceItem) && isObject(predicate)) {
		return Ok(predicate).every((key) => {
			let mods = [];

			if (key.startsWith("$")) mods.push(key);

			if (isObject(predicate[key]) && !isEmptyObject(predicate[key])) {
				Ok(predicate[key]).forEach((subKey) => {
					if (subKey.startsWith("$")) {
						mods.push(subKey);
					}
				});
			}

			if (mods.length) {
				let allMatch = true;

				mods.forEach((mod) => {
					let res = operators[mod](sourceItem, predicate);
					if (isArray(res) && !res.length) allMatch = false;
					else if (!res) allMatch = false;
				});

				return allMatch;
			}

			return safeHasOwnProperty(sourceItem, key) && checkAgainstPredicate(sourceItem[key], predicate[key]);
		});
	}

	return sourceItem === predicate;
};
