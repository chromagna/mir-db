import { idk } from "./index.mjs";
import { ops } from "./index.mjs";
import { processMutationOperators } from "./operators/index.mjs";
import { applyQueryOptions } from "./options.mjs";
import { getDefaultQueryOptions } from "./options.mjs";
import { returnFound } from "./returnFound.mjs";
import { ensureArray } from "./utils.mjs";
import { isUndefined } from "./utils.mjs";

export const update = async (data, query, params, options, db) => {
	options = getDefaultQueryOptions(options);
	query = ensureArray(query);
	let mutated = [];

	for (const q of query) {
		let itemsToMutate = [];
		itemsToMutate = returnFound(data, q, idk);
		itemsToMutate = ensureArray(itemsToMutate);

		mutated.push(...await processMutationOperators(itemsToMutate, params, q, db));
	}

	mutated = applyQueryOptions(mutated, options);
	db.postOperation(ops.UPDATE);

    if (db.options.log) {
        console.log("UPDATED:");
        console.log(JSON.stringify(mutated, null, 4));
    }

	return mutated;
};
