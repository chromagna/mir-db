import { isBrowserEnvironment } from "../utils.mjs";

export const localStorageAdapter = (storagePath, dbName) => {
	let _storagePath = storagePath;
	let _dbName = dbName;

	return {
        name: "localStorageAdapter",
		read: function () {
			if (!isBrowserEnvironment()) return;
			let data = Object.assign({}, JSON.parse(localStorage.getItem(`mdb__${_dbName}`)) || {});
			return data;
		},
		write: function (data) {
			if (!isBrowserEnvironment()) return;
			localStorage.setItem(`mdb__${_dbName}`, JSON.stringify(data));
		},
	};
};
