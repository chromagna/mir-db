import fs from "fs-extra";
import { join } from "path";
import { isNodeEnvironment } from "../utils.mjs";

function EventEmitter() {
	const eventRegister = {};

	const on = (name, fn) => {
		if (!eventRegister[name]) eventRegister[name] = [];
		eventRegister[name].push(fn);
	};

	const trigger = (name) => {
		if (!eventRegister[name]) return false;
		eventRegister[name].forEach((fn) => fn.call());
	};

	const off = (name, fn) => {
		if (eventRegister[name]) {
			const index = eventRegister[name].indexOf(fn);
			if (index >= 0) eventRegister[name].splice(index, 1);
		}
	};

	return {
		on,
		trigger,
		off,
	};
}

const fifo = () => {
    let elements = [];

    return {
        push: function (...args) {
            return elements.push(...args);
        },
        shift: function () {
            return elements.shift();
        },
        length: function () {
            return elements.length;
        },
    }
};

export const fileAdapter = (storagePath, dbName) => {
	let _storagePath = storagePath;
	let _fileName = !dbName.endsWith(".json") ? `${dbName}.json` : dbName;
    let _queue = fifo();

	if (!fs.existsSync(_storagePath)) fs.ensureDirSync(_storagePath, { mode: 0o2775 });

	_fileName = join(_storagePath, _fileName);

	return {
		name: "fileAdapter",
		read: async function () {
            return new Promise(resolve => {
                if (!isNodeEnvironment()) return;
                let data;
                try {
                    if (fs.existsSync(_fileName)) {
                        data = Object.assign({}, JSON.parse(fs.readFileSync(_fileName, "utf-8")) || {});
                    } else {
                        data = {};
                    }
                } catch (e) {
                    data = {};
                }
                resolve(data);
            });
        },
        write: async function (data) {
            _queue.push([async (d) => {
                if (!isNodeEnvironment()) return;
                await writeJSON(d, _fileName);
            }, data]);

            while (_queue.length()) {
                let ar = _queue.shift();
                await ar[0](ar[1]);
            }
		},
	};
};

const writeJSON = async (data, ...args) => {
	const env = process.env.NODE_ENV || "development";
	const indent = env === "production" ? 0 : 4;
	const result = JSON.stringify(data, null, indent);

	return await write(result, ...args);
};

const write = async (data, ...args) => {
	const path = join(...args);
	return new Promise((res, rej) => {
		fs.writeFileSync(path, data, (err) => {
			return err ? rej(err) : res();
		});
	});
};
