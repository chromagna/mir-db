import { checkAgainstPredicate } from "./helpers.mjs";
import { isUndefined, isArray, isEmptyObject, isObject, Ok } from "./utils.mjs";

export const insertObject = (source, predicate, objectToInsert, isBefore) => {
    if (isUndefined(source)) return undefined;

    const processObject = (item) => {
        if (!isObject(item)) return item;

        let itemClone = { ...item };

        Ok(itemClone).forEach((key) => {
            if (isObject(itemClone[key]) || isArray(itemClone[key])) {
                itemClone = {
                    ...itemClone,
                    [key]: insertObject(itemClone[key], predicate, objectToInsert, isBefore),
                };
            }
        });

        return itemClone;
    };

    const processArray = (sourceArray) => {
        const indexes = [];
        const sourceClone = sourceArray.map((item, index) => {
            const processedItem = processObject(item);

            if (checkAgainstPredicate(processedItem, predicate)) indexes.push(index);

            return processedItem;
        });

        if (indexes.length > 0) {
            for (let i = 0; i < indexes.length; i += 1) {
                sourceClone.splice(indexes[i] + i + (isBefore ? 0 : 1), 0, objectToInsert);
            }
        }

        return sourceClone;
    };

    if ((isArray(source) || isObject(source)) && !isEmptyObject(predicate) && !isEmptyObject(objectToInsert)) {
        return isArray(source) ? processArray(source) : processObject(source);
    }

    return source;
};

export const insertObjectBefore = (source, predicate, objectToInsert) => {
    return insertObject(source, predicate, objectToInsert, true);
};

export const insertObjectAfter = (source, predicate, objectToInsert) => {
    return insertObject(source, predicate, objectToInsert, false);
};
