import _ from "lodash";
import { fileAdapter } from "./adapters/file.mjs";
import { localStorageAdapter } from "./adapters/localStorage.mjs";
import { find } from "./find.mjs";
import { getDefaultDBOptions } from "./options.mjs";
import { update } from "./update.mjs";
import { isBrowserEnvironment } from "./utils.mjs";
import { isEmptyObject } from "./utils.mjs";
import { ensureArray, isArray, isNodeEnvironment } from "./utils.mjs";

export { fileAdapter, localStorageAdapter };

export const ops = Object.freeze({
	DROP: "DROP",
	INSERT: "INSERT",
	UPDATE: "UPDATE",
	REMOVE: "REMOVE",
});

let events = {};

export let idk = "_id";

export const makeDistinctByKey = (arr, key) => {
	let map = new Map();
	let val;
    arr = ensureArray(arr);
	let unique = arr.filter((el) => {
		val = map.get(el[key]);
		if (val) {
			if (el[key] != val) {
				map.delete(el[key]);
				map.set(el[key], el[key]);
				return true;
			} else {
				return false;
			}
		}
		map.set(el[key], el[key]);
		return true;
	});
	return unique;
};

export const mdb = async (storagePath = ".data", dbName = "main", options = {}, storageAdapters = []) => {
	let _storagePath = storagePath;
	let _dbName = dbName;
	let _options = {};
	let _data = {};
	let _storageAdapters = [];

    await (async () => {
		storageAdapters = ensureArray(storageAdapters);
		_options = getDefaultDBOptions(options);
        if (!storageAdapters.length) {
            if (isBrowserEnvironment()) storageAdapters.unshift(localStorageAdapter);
            if (isNodeEnvironment()) storageAdapters.unshift(fileAdapter);
        }
		for (const adapter of storageAdapters) _storageAdapters.push(adapter(_storagePath, _dbName));
        _data = await _storageAdapters[0].read();
		events[_dbName] = [];
	})();

	const prepPrivateIDMap = () => {
		if (!_data["__private"]) _data["__private"] = {};
		if (!_data["__private"]["__id_map"]) _data["__private"]["__id_map"] = {};
	};

	return {
		get options() {
			return _options;
		},
		drop: async function () {
			_data = {};
			await this.postOperation(ops.DROP);
			return _data;
		},
		on: function (handler) {
			events[_dbName].push(handler);
		},
		postOperation: async function (op, triggerSync = true) {
			events[_dbName].map(async (ev) => await ev(op));
			triggerSync && await this.handleAutosync();
		},
        handleAutosync: async function () {
            if (_options.autosync) return await this.sync();
			return false;
		},
		insert: async function (documents) {
			if (!isArray(documents)) documents = [documents];
			if (!documents.length) return documents;
			documents.forEach((doc) => {
				if (doc && doc != null) {
					const _id = doc[idk] ? doc[idk] : doc[idk] === 0 ? 0 : this.getID();
					if (_options.integerIds) {
						let id = _data["__private"]["__nextID"]++;
						doc["id"] = id;
						_data[_id] = Object.assign({ _id, id }, doc);
						prepPrivateIDMap();
						_data["__private"]["__id_map"][`${id}`] = { uuid: _id };
					} else {
						if (doc.id) {
							_data[_id] = Object.assign({ _id }, doc);
							prepPrivateIDMap();
							_data["__private"]["__id_map"][`${doc.id}`] = { uuid: _id };
						} else {
							_data[_id] = Object.assign({ _id }, doc, _data[_id]);
						}
					}
					doc[idk] = _id;
					if (_options.timestamps) _data[_id]["createdAt"] = Date.now();
				}
			});

			await this.postOperation(ops.INSERT);

            if (_options.log) {
                console.log("INSERTED:");
                console.log(JSON.stringify(documents, null, 4))
            }

			return documents;
		},
        find: function(query, options) {
            return find(_data, query, options, _options, this);
        },
		update: async function (query, params, options = {}) {
			return await update(_data, query, params, options, this);
		},
        upsert: async function (query, params, options = {}) {
            const u = await update(_data, query, params, options, this);
            if (!u.length) {
                const i = await this.insert(query);
                return await update(i, query, params, options, this);
            }
            return u;
        },
		remove: async function (query, options = {}) {
            let found = this.find(query, { ...options, clonedData: false });
			for (const f of found) {
				delete _data[f[idk]];
				if (_options.integerIds && f[idk]) {
					if (f["id"]) {
						delete _data["__private"]["__id_map"][`${f["id"]}`];
					}
                }

				if (!_options.integerIds && f["id"] && f[idk]) {
					delete _data["__private"]["__id_map"][`${f["id"]}`];
				}
            }
			await this.postOperation(ops.REMOVE);
			return found;
        },
        mergeShallow: async function (id, data) {
            // It's very possible that _data[id] no longer exists
            // thanks to one of the mutation operators.
            // if it's undefined, then we've nothing to merge.
            if (!id) return;
            if (typeof _data[id] === "undefined") return;
            Object.assign(_data[id], (isEmptyObject(data) ? {} : data))
            await this.postOperation(ops.UPDATE);
		},
		mergeDeep: async function (id, data) {
			if (!id) return;
			_data[id] = _.merge(_data[id], data);
			await this.postOperation(ops.UPDATE);
		},
		getID: function () {
			if (_options.integerIds) {
				if (!_data["__private"]) {
					_data["__private"] = {};
					_data["__private"]["__nextID"] = 1;
				}

				return this.uuidv4();
			} else {
				return this.uuidv4();
			}
		},
		uuidv4: function () {
			return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
				let r = (Math.random() * 16) | 0,
					v = c == "x" ? r : (r & 0x3) | 0x8;
				return v.toString(16);
			});
		},
		sync: async function () {
            _storageAdapters.forEach(async (storage) => {
                try {
					await storage.write(_data);
				} catch (e) {
					throw new Error(e);
				}
			});
		},
	};
};
