import { checkAgainstPredicate } from "./helpers.mjs";
import { Ok } from "./utils.mjs";
import { isUndefined } from "./utils.mjs";
import { isArray, isEmptyObject, isObject, safeHasOwnProperty } from "./utils.mjs";

export const changeProps = (source, predicate, replaceProps, createNewProperties = false) => {
    if (isUndefined(source)) return undefined;

    const processObject = (item) => {
        if (!isObject(item)) return item;

        let itemClone = { ...item };

        if (checkAgainstPredicate(itemClone, predicate)) {
            Ok(replaceProps).forEach((key) => {
                if (createNewProperties) {
                    itemClone = {
                        ...itemClone,
                        [key]: replaceProps[key],
                    };
                } else {
                    if (safeHasOwnProperty(itemClone, key)) {
                        itemClone = {
                            ...itemClone,
                            [key]: replaceProps[key],
                        };
                    }
                }
            });
        }

        Ok(itemClone).forEach((key) => {
            if (isObject(itemClone[key]) || isArray(itemClone[key])) {
                itemClone = {
                    ...itemClone,
                    [key]: changeProps(itemClone[key], predicate, replaceProps, createNewProperties),
                };
            }
        });

        return itemClone;
    };

    if ((isArray(source) || isObject(source)) && !isEmptyObject(predicate) && !isEmptyObject(replaceProps)) {
        return !isArray(source) ? processObject(source) : source.map(item => processObject(item));
    }

    return source;
};
