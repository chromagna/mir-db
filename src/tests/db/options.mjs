import * as assert from "assert";
import { find } from "../../find.mjs";
import { getdb, removeids } from "../common.mjs";
import { group, test } from "../runner.mjs";

group("options", () => {
    test("sort one property ascending", async () => {
        const db = await getdb();
        await db.insert({number: 1});
        await db.insert({number: 4});
        await db.insert({number: 5});
        await db.insert({number: 2});
        await db.insert({number: 3});

        let found = db.find({number: {$lt: 10}}, { sort: { number: 1 } });

        found = removeids(found);

        assert.deepStrictEqual(found, [
            {number: 1},
            {number: 2},
            {number: 3},
            {number: 4},
            {number: 5},
        ]);
    });

    test("sort one property descending", async () => {
        const db = await getdb();
        await db.insert({number: 1});
        await db.insert({number: 4});
        await db.insert({number: 5});
        await db.insert({number: 2});
        await db.insert({number: 3});

        let found = db.find({number: {$lt: 10}}, { sort: { number: 0 } });
        found = removeids(found);

        assert.deepStrictEqual(found, [
            {number: 5},
            {number: 4},
            {number: 3},
            {number: 2},
            {number: 1},
        ]);
    });

    test("sort multiple properties ascending and descending, numeric and alphanumeric", async () => {
		const db = await getdb();
		await db.insert({ name: "Deanna Troi", age: 28 });
		await db.insert({ name: "Worf", age: 24 });
		await db.insert({ name: "Xorf", age: 24 });
		await db.insert({ name: "Zorf", age: 24 });
		await db.insert({ name: "Jean-Luc Picard", age: 59 });
		await db.insert({ name: "William Riker", age: 29 });

        let found = db.find({}, {sort: {age: 1, name: -1}});
        found = removeids(found);

        assert.deepStrictEqual(found, [
			{ name: "Zorf", age: 24 },
			{ name: "Xorf", age: 24 },
			{ name: "Worf", age: 24 },
			{ name: "Deanna Troi", age: 28 },
			{ name: "William Riker", age: 29 },
			{ name: "Jean-Luc Picard", age: 59 },
		]);

	});
});
