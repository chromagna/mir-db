import * as assert from "assert";
import { group, test } from "../runner.mjs";
import { getdb, removeids } from "../common.mjs";

group("handlers", () => {
    test("fires a handler", async () => {
        let o = null;
        const handler = (op) => o = op;

        const db = await getdb();
        db.on(handler);
        await db.insert({a: 1, b: 2});

        for(;!o;await sleep(1));

        assert.strictEqual(o, "INSERT");
    });
});
