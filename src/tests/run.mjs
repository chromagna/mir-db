import { runSuite } from "./runner.mjs";

async function main() {
    await runSuite("./db/find.mjs");
    await runSuite("./db/handlers.mjs");
    await runSuite("./db/operators.mjs");
    await runSuite("./db/options.mjs");
    await runSuite("./db/upsert.mjs");
    await runSuite("./db/remove.mjs");
    await runSuite("./db/integerIds.mjs");
}

main();
