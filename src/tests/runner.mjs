export let modules = [];

let groups = [];
let tests = [];

export const group = (name, fn) => groups.push({ name, fn });
export const test = (name, fn) => tests.push({ name, fn });

export const runSuite = async (modulePath, suiteName = "") => {
    groups = [];
    tests = [];
    const mod = await import(modulePath);
    run(suiteName);
};

const run = async (suiteName) => {
    let allFailures = 0;
    let allPasses = 0;

    for (const [_, g] of groups.entries()) {
        let groupPasses = 0;
        let groupFailures = 0;

        g.fn();
        console.log(` ${bright}GROUP${reset} ${g.name}`);
        for (const t of tests) {
            try {
                await t.fn();
                groupPasses += 1;
                console.log(` ${greenbr}PASS${reset}`, t.name);
            } catch (e) {
                groupFailures += 1;
                console.log(` ${redbr}FAIL${reset}`, t.name);
                console.log(e);
            }
        }

        let out = " ";
        out += `${bright}PASSES${reset}`;
        if (groupPasses) out += ` ${greenbr}${groupPasses}${reset}`;
        else out += ` ${groupPasses}`;

        out += ` | ${bright}FAILURES${reset}`;
        if (groupFailures) out += ` ${redbr}${groupFailures}${reset}`;
        else out += ` ${greenbr}${groupFailures}${reset}`;

        if (groupPasses && !groupFailures) out += " 🎉";

        console.log(out);

        allPasses += groupPasses;
        allFailures += groupFailures;
    }
};

const reset = "\x1b[0m";
const bright = "\x1b[1m";
const green = "\x1b[32m";
const greenbr = "\x1b[1m\x1b[32m";
const red = "\x1b[31m";
const redbr = "\x1b[1m\x1b[31m";
