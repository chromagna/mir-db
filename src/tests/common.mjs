import { mdb } from "../index.mjs";
import { ensureArray } from "../utils.mjs";

/**
 * 
 * @param {boolean} log Enable verbose logging for operations on this db
 * @param {boolean} integerIds Enable integerIds
 * @returns 
 */
export const getdb = async (log = false, integerIds = false) => {
    const db = await mdb(".data", "testing", { autosync: true, log, integerIds });
    await db.drop();
    return db;
};

/**
 * @param {any} results Returned db results.
 * @param {boolean} removeIntegerIds Whether or not to remove the `id` property
 */
export const removeids = (results, removeIntegerIds = true) => {
    results = ensureArray(results);
    if (results?.[0]?.__id_map) results.shift();
    for (const o of results) {
        o._id && delete o._id;
        if (removeIntegerIds) o.id && delete o.id;
    }
    return results;
};
