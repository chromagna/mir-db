# mir-db

[toc]

# Things worth mentioning up front

- This is a very simple, non-transactional database.
- There are no indexes.
- This is designed for small to medium-sized applications.
- There should only be one handle to a database at a time.
- Data returned from a query is a deep clone of what's actually in the database. Feel free to directly mutate query results.

# When should this be used?

Whenever you need a database that:

- Is extremely lightweight
- Is reliable and fully tested
- Has little to no boilerplate setup
- Has thorough documentation

# What's next?

## Projection

Projection will allow you to define which properties are present in query results.

In the query below, only the `status` field would be returned. The `_id` field must be explicitly
removed because, by default, it is included in all queries.

```javascript
db.find({ status: { $like: "Pending" } }, { project: { _id: 0, status: 1 } });
```

# Quickstart

## Install

```bash
npm install gitlab:chromagna/mir-db
```

## Use

```javascript
import { mdb } from "mir-db";

const usersDb = await mdb(".data", "users");

await usersDb.insert({ username: "chromagna" });
```

# find, update, insert, remove

`find`, `update`, `insert`, and `remove` are the four main methods of mirdb.

`find` is used to (unsurprisingly) find documents using a given query.

`insert` is used to (also unsurprisingly) insert documents.

`upsert` will update documents if found. If they are not found, they are inserted and then updated using whatever operators were supplied.

`update` and `remove` both rely on `find` to search for objects to operate on.

# find

`find` always returns an array. If one value is found, an array is returned. If nothing is found, an empty array is returned.
There is no `findOne` method, but if you really need this behavior then you can easily implement one yourself:

```javascript
const findOne = (query, options = {}) => db.find(query, options)[0];
```

## Find a document

Given documents:

```javascript
[
    {
        "a": 1,
        "b": {
            "c": {
                "d": {
                    "e": 5
                },
            },
        },
        "_id": "e7ebb313-6922-4ebd-96df-ff86bcded8b2"
    }
]
```

You can find this entry with:

```javascript
db.find({ a: 1 });
```

Or:

```javascript
db.find({ e: 5 });
```

By default, deep matching is enabled. That's why the second query will work. You can disable deep matching on a per-query basis and only match top level properties by passing `"deep": false`, as part of the query options.

This query will not find the above document:

```javascript
db.find({ e: 5 }, { deep: false });
```

## Finding multiple documents

Given documents:

```javascript
[
    {
        "a": {
            "b": 1
        },
        "_id": "a80d3da5-11cc-4007-a602-aa113ed35885"
    },
    {
        "x": {
            "b": 1
        },
        "_id": "d0b14ad3-f01a-4fc3-b717-fbe78ec4c36f"
    }
]
```

Query:
```javascript
db.find({"b": 1});
```

Returns:

```javascript
[
    {
        "_id": "a80d3da5-11cc-4007-a602-aa113ed35885",
        "a": {
            "b": 1
        }
    },
    {
        "_id": "d0b14ad3-f01a-4fc3-b717-fbe78ec4c36f",
        "x": {
            "b": 1
        }
    }
]
```

---

Wrap queries in an array.

Given documents:
```javascript
[
    {
        "x": {
            "a": 1
        },
        "_id": "699a9c43-839c-492b-9d7c-b63c12f4295b"
    },
    {
        "y": {
            "b": 1
        },
        "_id": "175bdc18-38ad-40e2-b634-5348e6f85f59"
    }
]
```

Query:
```javascript
db.find([{"a": 1}, {"b": 1}]);
```

Returns:
```javascript
[
    {
        "_id": "699a9c43-839c-492b-9d7c-b63c12f4295b",
        "x": {
            "a": 1
        }
    },
    {
        "_id": "175bdc18-38ad-40e2-b634-5348e6f85f59",
        "y": {
            "b": 1
        }
    }
]
```

## Finding documents nested in an array

Given documents:

```javascript
[
    {
        "a": {
            "b": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 3
                }
            ]
        },
        "_id": "fd907d01-fa3f-4091-8363-2b20a865a480"
    }
]
```

Query: **note the omission of array brackets**
```javascript
db.find({"b": {"c": 2}});
```

Returns:
```javascript
[
    {
        "_id": "fd907d01-fa3f-4091-8363-2b20a865a480",
        "a": {
            "b": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 3
                }
            ]
        }
    }
]
```

## Find documents using deep specification

Given documents:

```javascript
[
    {
        "a": 1,
        "_id": "8353e01c-3345-4f47-86ec-1c3f0c26f5bc"
    },
    {
        "a": 2,
        "_id": "0abaf820-816b-4c3d-8d98-655a8adb1598"
    },
    {
        "a": 2,
        "b": "bbb",
        "c": [
            "a",
            "b",
            "c"
        ],
        "d": {
            "e": {
                "f": "f"
            }
        },
        "_id": "4d433cf4-4ef8-4a54-b725-9fdbdf64fa35"
    }
]
```

Query:
```javascript
db.find({"a": 2, "d": {"e": {"f": "f"}}});
```

Result:
```javascript
[
    {
        "_id": "4d433cf4-4ef8-4a54-b725-9fdbdf64fa35",
        "a": 2,
        "b": "bbb",
        "c": [
            "a",
            "b",
            "c"
        ],
        "d": {
            "e": {
                "f": "f"
            }
        }
    }
]
```

## Specifying a returnKey

By default, the returnKey is `_id`. You can change this.

For all objects that match, no matter how deep they are (assuming deep == true),
the matched object is added to the result set where returnKey specifies the
topmost level of the result.

An example will explain this much clearer than I am able to:

Given:
```javascript
[
    {
        "a": 1,
        "name": "foo",
        "b": {
            "a": 2,
            "name": "bar",
            "b": {
                "a": 1,
                "name": "baz"
            }
        },
        "_id": "21997703-7110-4daf-9a6b-4f961a429f22"
    }
]
```

### Without a returnKey

Query:
```javascript
db.find({ a: 1 });
```

Result:
```javascript
[
    {
        "_id": "21997703-7110-4daf-9a6b-4f961a429f22",
        "a": 1,
        "name": "foo",
        "b": {
            "a": 2,
            "name": "bar",
            "b": {
                "a": 1,
                "name": "baz"
            }
        }
    }
]
```

### With a returnKey

Query:
```javascript
db.find({ a: 1 }, { returnKey: "a" });
```

Result:
```javascript
[
    {
        "_id": "21997703-7110-4daf-9a6b-4f961a429f22",
        "a": 1,
        "name": "foo",
        "b": {
            "a": 2,
            "name": "bar",
            "b": {
                "a": 1,
                "name": "baz"
            }
        }
    },
    {
        "a": 1,
        "name": "baz"
    }
]
```

## Sorting query results

Given:
```javascript
[
    {
        "name": "Deanna Troi",
        "age": 28,
        "_id": "371f5215-c542-4d40-8724-a26e401ca344"
    },
    {
        "name": "Worf",
        "age": 24,
        "_id": "7e34339b-b3bf-4796-9820-7389f2b0c601"
    },
    {
        "name": "Xorf",
        "age": 24,
        "_id": "759e11da-52dd-4ab7-bfa0-8bd30c3390c4"
    },
    {
        "name": "Zorf",
        "age": 24,
        "_id": "628b6855-fd91-48ee-8b02-63c8055d7775"
    },
    {
        "name": "Jean-Luc Picard",
        "age": 59,
        "_id": "a515d063-72aa-49cf-b4b0-94165a3839d5"
    },
    {
        "name": "William Riker",
        "age": 29,
        "_id": "d65c43dd-1aa6-44cc-b449-8f9228c7f9ec"
    }
]
```

Let's say we want to sort these people by name in descending order, but also
by their age in ascending order:

Query:
```javascript
db.find({}, { sort: { age: 1, name: -1 } });
```

Result:
```javascript
[
    {
        "_id": "628b6855-fd91-48ee-8b02-63c8055d7775",
        "name": "Zorf",
        "age": 24
    },
    {
        "_id": "759e11da-52dd-4ab7-bfa0-8bd30c3390c4",
        "name": "Xorf",
        "age": 24
    },
    {
        "_id": "7e34339b-b3bf-4796-9820-7389f2b0c601",
        "name": "Worf",
        "age": 24
    },
    {
        "_id": "371f5215-c542-4d40-8724-a26e401ca344",
        "name": "Deanna Troi",
        "age": 28
    },
    {
        "_id": "d65c43dd-1aa6-44cc-b449-8f9228c7f9ec",
        "name": "William Riker",
        "age": 29
    },
    {
        "_id": "a515d063-72aa-49cf-b4b0-94165a3839d5",
        "name": "Jean-Luc Picard",
        "age": 59
    }
]
```

# upsert

The syntax for `upsert` is identical to the syntax of `update`.

```javascript
db.upsert({ first: "Jean-Luc" }, { $set: { last: "Picard" } });
```

When the above query is executed, if a document cannot be found with a
`first` property that matches "Jean-Luc", one is created:

```javascript
{
    first: "Jean-Luc"
}
```

Then, the supplied operators are evaluated against the inserted document:

```javascript
{
    first: "Jean-Luc",
    last: "Picard"
}
```

When deciding what operators to use with `upsert`, remember that certain mutation-type operators
are really the only ones that make sense, like `$set`.

# $operators

$operators are what make mir-db interesting and powerful.

This is a pretty typical $operator signature:

```javascript
db.update({ queryProp: "queryVal" }, { $operator: /* operator value */ });
```

Sometimes the value passed to an $operator is an array. Sometimes it's an object. Sometimes it's a string, number, boolean, etc. Read more about the specific $operator to understand what it expects to receive.

## Boolean operators

Boolean $operators are used within a find or update query to narrow down search
results based on the result of the boolean expression.

Boolean operators include:
- $and
- $or
- $like
- $fn
- $equals
- $excludes
- $excludesAll
- $excludesAny
- $includesAll
- $includesAny
- $gt
- $gte
- $lt
- $lte
- $in
- $nin

### $eqeqeq

`$eqeqeq` is functionally equivalent to `===`.

Given documents:
```javascript
[
    {
        "foo": "bar",
        "_id": "1a443641-07eb-4c9a-b5ee-21cc59c54698"
    }
]
```

Query:
```javascript
db.find({ foo: { $eqeqeq: "bar" } });
```

Result:
```javascript
[
    {
        "_id": "1a443641-07eb-4c9a-b5ee-21cc59c54698",
        "foo": "bar"
    }
]
```

---

Given:
```javascript
[
    {
        "foo": 5,
        "_id": "c190725e-b116-4036-b87a-f4e31b55776b"
    }
]
```

Query:
```javascript
db.find({"foo": {"$eqeqeq": "5"}});
```

Result:
```javascript
[]
```

#### Using with update

Given:
```javascript
[
    {
        "foo": 12,
        "hello": "me",
        "_id": "e6fdcc8e-745f-4483-90c6-98223f36961a"
    }
]
```

Query:
```javascript
db.update({ foo: { $eqeqeq: 12 } }, { $set: { hello: "world" } });
```

Result:
```javascript
[
    {
        "_id": "e6fdcc8e-745f-4483-90c6-98223f36961a",
        "foo": 12,
        "hello": "world"
    }
]
```

### $like

$like is a string .includes() match, e.g.: `"foo".includes("oo");`. It is case sensitive.
There may be a query option for sensitivity in the future, but for now there is not.

Given:
```javascript
[
    {
        "foo": {
            "bar": "baz"
        },
        "_id": "b6f41c81-b9b8-4652-9cd2-7d1c14eb9899"
    },
    {
        "aaa": {
            "bar": "baaaaaz"
        },
        "_id": "d5894196-921c-47ab-ad5f-b713baa67bd9"
    }
]
```

Query:
```javascript
db.find({"bar": {"$like": "az"}});
```

Result:
```javascript
[
    {
        "_id": "b6f41c81-b9b8-4652-9cd2-7d1c14eb9899",
        "foo": {
            "bar": "baz"
        }
    },
    {
        "_id": "d5894196-921c-47ab-ad5f-b713baa67bd9",
        "aaa": {
            "bar": "baaaaaz"
        }
    }
]
```

#### Array match

Given:
```javascript
[
    {
        "foo": {
            "bar": "baz"
        },
        "_id": "3eeec472-6004-49b2-9de1-100384665643"
    },
    {
        "hello": {
            "world": {
                "bar": "boo"
            }
        },
        "_id": "dbc4b0f4-f1f4-4ae2-af02-f67e84c142cd"
    }
]
```

Query:
```javascript
db.find({"bar": {"$like": ["ba", "bo"]}});
```

Result:
```javascript
[
    {
        "_id": "3eeec472-6004-49b2-9de1-100384665643",
        "foo": {
            "bar": "baz"
        }
    },
    {
        "_id": "dbc4b0f4-f1f4-4ae2-af02-f67e84c142cd",
        "hello": {
            "world": {
                "bar": "boo"
            }
        }
    }
]
```

#### Using with update

Given:
```javascript
[
    {
        "foo": {
            "bar": "baz"
        },
        "_id": "8b8b484a-676e-475f-a435-a83d44d445e9"
    }
]
```

Query:
```javascript
db.update({ bar: { $like: "z" } }, { $set: { bar: "boo" } });
```

Result:
```javascript
[
    {
        "_id": "8b8b484a-676e-475f-a435-a83d44d445e9",
        "foo": {
            "bar": "boo"
        }
    }
]
```

### $lt $gt $lte $gte

Given:
```javascript
[
    {
        "a": 1,
        "b": 5,
        "_id": "2d3111b3-4d8d-4b19-8bc4-49c1ccfd4266"
    },
    {
        "a": 2,
        "b": 3,
        "_id": "56323e5a-dcf9-4b36-845c-82b9d3cd6e14"
    },
    {
        "a": 4,
        "b": 1,
        "_id": "4706595a-e31c-48a1-b6e8-1b719b7e4571"
    }
]
```

Let's cover all of these operators in one example. To find the first object, we can
use any of the following queries:

Query:
```javascript
// $lt
db.find({ a: { $lt: 2 } });

// $lte
db.find({ a: { $lte: 1 } });

// $gt
db.find({ b: { $gt: 3 } });

// $gte
db.find({ b: { $gte: 5 } });

// $lt $gt
db.find({ a: { $lt: 2}, b: { $gt: 4 } });
```

For each of those queries, the result would be the same:

Result:
```javascript
[
    {
        "a": 1,
        "b": 5,
        "_id": "2d3111b3-4d8d-4b19-8bc4-49c1ccfd4266"
    },
]
```

### $in

The $in operator accepts an array of values. If the matched property's value exists in the provided array,
the object matches.

Given:
```javascript
[
    {
        "number": 1,
        "_id": "efce214b-5eb9-4b16-af41-6f970eabbe46"
    },
    {
        "number": 2,
        "_id": "513517b4-4998-48c2-8a05-7e8b08e4ca57"
    },
    {
        "number": 3,
        "_id": "ea0fd598-c6b9-416e-8edc-b6f94c68ba2f"
    }
]
```

Let's find all objects where number is 1 or 2.

Query:
```javascript
db.find({ number: { $in: [1, 2] } });
```

Result:
```javascript
[
    {
        "number": 1,
        "_id": "efce214b-5eb9-4b16-af41-6f970eabbe46"
    },
    {
        "number": 2,
        "_id": "513517b4-4998-48c2-8a05-7e8b08e4ca57"
    }
]
```

### $nin

Given:
```javascript
[
    {
        "number": 1,
        "_id": "fda62d62-d86f-4f4d-8c22-7ea201686c47"
    },
    {
        "number": 2,
        "_id": "5e91662b-2d0c-4844-a568-f3bbb6891c02"
    },
    {
        "number": 3,
        "_id": "e1c8dff7-f855-4fb2-b684-1902297be950"
    }
]
```

Let's find all numbers that are not 1 or 2.

Query:
```javascript
db.find({ number: { $nin: [1, 2] } });
```

Result:
```javascript
[
    {
        "number": 3,
        "_id": "e1c8dff7-f855-4fb2-b684-1902297be950"
    }
]
```

The $nin operator accepts an array of values. If the matched property's value does not exist in the provided array,
the object matches.

### $fn

The $fn operator passes the value of a property to a function and expects that function
to return a boolean value. If true, the property is considered a match.

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3,
            4
        ],
        "_id": "df6c3308-2cc8-479c-a0a6-8c5c5e097447"
    },
    {
        "numbers": [
            3,
            4,
            5,
            6
        ],
        "_id": "9996c37d-67e3-41dd-9e72-c2b27381cd2a"
    }
]
```

Query:
```javascript
function includesTwo(val) {
    return val.includes(2);
}

db.find({ numbers: { $fn: includesTwo } });
```

Result:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3,
            4
        ],
        "_id": "df6c3308-2cc8-479c-a0a6-8c5c5e097447"
    },
]
```

### $or

Given:
```javascript
[
    {
        "number": 2,
        "_id": "3ce2824d-9724-4a29-9c36-fca71580c472"
    },
    {
        "number": 4,
        "_id": "5e0ddb57-7ee5-4682-b4eb-84ba4857d04f"
    },
    {
        "number": 5,
        "_id": "3c9044ac-6f2e-4889-9018-208be4212a0f"
    },
    {
        "number": 7,
        "_id": "381392f3-fbc7-4eb0-94eb-3e967a2e97f2"
    },
    {
        "number": 15,
        "_id": "f6d69fd6-eb36-477a-9ac1-db3be74da9db"
    }
]
```

Let's find all results where number is `15` or even. We'll use the $fn operator
to help us find even numbers.

Query:
```javascript
function isEven(val) {
    return val % 2 == 0;
}

db.find({ $or: [{ number: { $eqeqeq: 15 } }, { number: { $fn: isEven } }] });
```

Result:
```javascript
[
    {
        "_id": "3ce2824d-9724-4a29-9c36-fca71580c472",
        "number": 2
    },
    {
        "_id": "5e0ddb57-7ee5-4682-b4eb-84ba4857d04f",
        "number": 4
    },
    {
        "_id": "f6d69fd6-eb36-477a-9ac1-db3be74da9db",
        "number": 15
    }
]
```

### $and

TODO

### $includesAll

$includesAll will return objects as long as the matched array includes all values provided in the array
passed to $includesAll.

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "1fbdc95e-6ea9-47db-bf39-fc58d1f4463a"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "8bc16518-c0c4-4e72-ac47-9f32ff8856e4"
    }
]
```

Query:
```javascript
db.find({ numbers: { $includesAll: [1, 2] } });
```

Result:
```javascript
[
    {
        "_id": "1fbdc95e-6ea9-47db-bf39-fc58d1f4463a",
        "numbers": [
            1,
            2,
            3
        ]
    }
]
```

### $includesAny

$includesAny will return objects as long as the matched array includes any of the values
provided in the array passed to $includesAny.

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "e6fd8a54-032d-4652-b873-8426ddf26c2e"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "084b3dd4-e27b-4a1a-974e-e2d43aa8f3c3"
    }
]
```

Query:
```javascript
db.find({ numbers: { $includesAny: [1, 6, 15, 100, 200, 0] } });
```

Result:
```javascript
[
    {
        "_id": "e6fd8a54-032d-4652-b873-8426ddf26c2e",
        "numbers": [
            1,
            2,
            3
        ]
    },
    {
        "_id": "084b3dd4-e27b-4a1a-974e-e2d43aa8f3c3",
        "numbers": [
            4,
            5,
            6
        ]
    }
]
```

### $excludesAll

$excludesAll will return objects as long as the matched array excludes ALL of the
values provided in the array passed to $excludesAll.

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "82cd8a89-09fb-497e-8431-fe5c889c256c"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "3014ef24-abc0-4b05-aa31-561006b29936"
    }
]
```

Neither `numbers` array excludes ALL of the numbers in the query below, so neither
of them will be returned.

Query:
```javascript
db.find({ numbers: { $excludesAll: [1, 2, 4] } });
```

Result:
```javascript
[]
```

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "0e762f47-b703-4b0e-8bdf-c56033ed5ba0"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "c4cd7b90-579e-4bd6-bdb9-0c8012dd5c61"
    }
]
```

Query:
```javascript
db.find({ numbers: { $excludesAll: [1, 2] } });
```

Result:
```javascript
[
    {
        "_id": "c4cd7b90-579e-4bd6-bdb9-0c8012dd5c61",
        "numbers": [
            4,
            5,
            6
        ]
    }
]
```

### $excludesAny

$excludesAny will return objects as long as the matched array excludes ANY of the
values provided in the array passed to $excludesAny.

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "7ca3fd9b-1fd8-435f-9942-914b08f51bc9"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "1ff79f89-5524-4f15-8a03-4769c4e2a1fc"
    }
]
```

The second `numbers` array excludes 1, so it will be returned.

Query:
```javascript
db.find({ numbers: { $excludesAny: [1] } });
```

Result:
```javascript
[
    {
        "_id": "1ff79f89-5524-4f15-8a03-4769c4e2a1fc",
        "numbers": [
            4,
            5,
            6
        ]
    }
]
```

Given:
```javascript
[
    {
        "numbers": [
            1,
            2,
            3
        ],
        "_id": "d9700f23-5d89-4755-b4b6-77f58bbd2807"
    },
    {
        "numbers": [
            4,
            5,
            6
        ],
        "_id": "9b486255-226a-45c5-a049-e7a457d9f36c"
    }
]
```

In this example, the first `numbers` array excludes 4, so it matches, and the second `numbers` array excludes 1, so it matches.

Query:
```javascript
db.find({ numbers: { $excludesAny: [1, 4] } });
```

Result:
```javascript
[
    {
        "_id": "d9700f23-5d89-4755-b4b6-77f58bbd2807",
        "numbers": [
            1,
            2,
            3
        ]
    },
    {
        "_id": "9b486255-226a-45c5-a049-e7a457d9f36c",
        "numbers": [
            4,
            5,
            6
        ]
    }
]
```

### Combining $or with $and and some other operators

Given documents:

```javascript
[
    {
        "a": 1,
        "b": 5,
        "_id": "48f0436e-b7c2-487d-9d76-b2cd04a67af3"
    },
    {
        "a": 3,
        "b": 7,
        "_id": "ff5b8bbd-e44e-4bdb-830c-1b50e2ccbc07"
    },
    {
        "a": 15,
        "b": 1,
        "_id": "96e71eee-e100-4ea0-9dd9-9aa1a0606cc8"
    },
    {
        "a": 25,
        "b": 2,
        "_id": "c2b6afdf-1ed3-4dc4-9a93-7d5837a2504f"
    }
]
```

To find all documents where [`a` is `3` OR `b` is `1`] AND [`a` is `15` OR `b` is [ $gte `7` and $lte `7` ]]

Query:

```javascript
db.find({
    $and: [
        { $or: [{ a: { $eqeqeq: 3 } }, { b: 1 }] },
        { $or: [{ a: 15 }, { b: { $gte: 7, $lte: 7 } }] }
    ],
});
```

Result:

```javascript
[
    {
        "_id": "ff5b8bbd-e44e-4bdb-830c-1b50e2ccbc07",
        "a": 3,
        "b": 7
    },
    {
        "_id": "96e71eee-e100-4ea0-9dd9-9aa1a0606cc8",
        "a": 15,
        "b": 1
    }
]
```

## Mutation operators

Mutation operators are used by `update`.

Use the boolean operators in the `update` query to get objects to mutate with mutation operators.

### $set

$set is used to modify the value of an existing property, and create properties with
the provided values if they do not already exist.

To avoid creating new properties if they do not already exist, use $change.

Given:
```javascript
[
    {
        "foo": {
            "bar": "baz"
        },
        "_id": "87af218d-25d3-465f-ba74-9c18060d9418"
    }
]
```

Query: **note that we're setting the property `baz` at the same level that matches the query**. In other words, `baz` will be a child of property `foo`, and not a sibling.

```javascript
db.update({ bar: "baz" }, { $set: { baz: "hmm" } });
```

Result:
```javascript
[
    {
        "_id": "87af218d-25d3-465f-ba74-9c18060d9418",
        "foo": {
            "bar": "baz",
            "baz": "hmm"
        }
    }
]
```

Let's expand a bit on what is meant by "**the level that matches the query**".
Take a look at this example to help illustrate how match level is decided:

Given:
```javascript
[
    {
        "bar": "boo",
        "foo": {
            "bar": "baz"
        },
        "_id": "df2f0793-a4f0-406f-95a2-0a0fa050806f"
    }
]
```

Query:
```javascript
db.update({ foo: {bar: "baz"  }}, { $set: { baz: "hmm" } });
```

Where would you expect the property, `baz`, to be created? A child of `foo`, or a sibling?

It will be a sibling, because the matched level is defined by the outermost level of the object that was used in the query. In that example, the outermost level begins with `{ foo:{}}`.

Result:
```javascript
[
    {
        "_id": "df2f0793-a4f0-406f-95a2-0a0fa050806f",
        "bar": "boo",
        "foo": {
            "bar": "baz"
        },
        "baz": "hmm"
    }
]
```

#### Changing properties of multiple results

Given:
```javascript
[
    {
        "name": "Fred",
        "_id": "3f366c87-c3e7-47e0-9d6f-15beb44ec241"
    },
    {
        "name": "Frank",
        "_id": "a0e82ef8-8f36-4a70-b96f-fe7267328f0b"
    },
    {
        "name": "Frodo",
        "_id": "17dee2b0-b960-4613-9e49-baf5c8d9fb70"
    }
]
```

Query:
```javascript
db.update([{ name: "Fred" }, { name: "Frodo" }], { $set: { num: 1 } });
```

Result:
```javascript
[
    {
        "_id": "3f366c87-c3e7-47e0-9d6f-15beb44ec241",
        "name": "Fred",
        "num": 1
    },
    {
        "_id": "a0e82ef8-8f36-4a70-b96f-fe7267328f0b",
        "name": "Frank"
    },
    {
        "_id": "17dee2b0-b960-4613-9e49-baf5c8d9fb70",
        "name": "Frodo",
        "num": 1
    }
]
```

Please note that $set will deeply modify properties if the query is loose enough. The following example illustrates this behavior.

Given:
```javascript
[
    {
        "foo": "bar",
        "name": "me",
        "baz": {
            "name": "me"
        },
        "_id": "c2a23d20-7012-41d9-b4de-8e65e5d72fbb"
    }
]
```

Query:
```javascript
db.update({ name: "me" }, { $set: { name: 12345 } });
```

Result:
```javascript
[
    {
        "_id": "c2a23d20-7012-41d9-b4de-8e65e5d72fbb",
        "foo": "bar",
        "name": 12345,
        "baz": {
            "name": 12345
        }
    }
]
```

#### Shorthand behavior

If you don't pass an object to $set, it uses the value you pass it as the new value for whatever properties match the query.

Note that this is quite different than the standard $set behavior, but still has its value.

Given:
```javascript
[
    {
        "foo": "bar",
        "name": "me",
        "baz": {
            "name": "foo"
        },
        "_id": "62845bc5-f0a8-40c5-a3f8-40aae966feb6"
    }
]
```

Query:
```javascript
db.update({ foo: "bar", name: "me" }, { $set: 12345 });
```

Result:
```javascript
[
    {
        "_id": "62845bc5-f0a8-40c5-a3f8-40aae966feb6",
        "foo": 12345,
        "name": 12345,
        "baz": {
            "name": "foo"
        }
    }
]
```

### $unset

$unset is used to delete properties if they exist.

#### Strict match

Given:
```javascript
[
    {
        "name": "Fred",
        "friend": "Frank",
        "_id": "16b36adb-57ac-4587-ac14-46f5ebc19971"
    }
]
```

Query:
```javascript
db.update({ name: "Fred" }, { $unset: { friend: "Frank" } });
```

Result:
```javascript
[
    {
        "name": "Fred",
        "_id": "16b36adb-57ac-4587-ac14-46f5ebc19971"
    }
]
```

#### Loose match

Given:
```javascript
[
    {
        "name": "Fred",
        "friend": "Frank",
        "_id": "16b36adb-57ac-4587-ac14-46f5ebc19971"
    }
]
```

Query:
```javascript
db.update({ name: "Fred" }, { $unset: "friend" });
```

Result:
```javascript
[
    {
        "name": "Fred",
        "_id": "16b36adb-57ac-4587-ac14-46f5ebc19971"
    }
]
```

#### Multiple removals, strict and loose

To unset multiple properties, pass them in an array.

Given:

```javascript
[
    {
        "name": "Fred",
        "friend": "Frank",
        "pets": [
            "Spot",
            "Felix"
        ],
        "_id": "00e160eb-6e2d-49e8-ac00-823c232d76c7"
    }
]
```

For this example, we want to unset `friend` only if it matches `Frank`, and remove
`pets` no matter what.

Query:
```javascript
db.update({ name: "Fred" }, { $unset: ["pets", { friend: "Frank" }] });
```


Result:
```javascript
[
    {
        "name": "Fred",
        "_id": "00e160eb-6e2d-49e8-ac00-823c232d76c7"
    }
]
```

### $change

$change is similar to $set, but $change will not create properties if they do not already exist.

Given:
```javascript
[
    {
        "foo": "bar",
        "_id": "bd5c985f-e722-4042-86de-1c12b3042f49"
    }
]
```

Query:
```javascript
db.update({ foo:"bar" }, { $change: { bar: "baz" } });
```

Result:
```javascript
[
    {
        "foo": "bar",
        "_id": "bd5c985f-e722-4042-86de-1c12b3042f49"
    }
]
```

### Arithmatic operators

#### $mult

Multiplies a property by a value.

Given:
```javascript
[
    {
        "number": 2,
        "_id": "a6c5c795-f000-447e-9557-a1721eed6ec3"
    }
]
```

Query:
```javascript
db.update({ number: 2 }, { $mult: { number: 5 } });
```

Result:
```javascript
[
    {
        "_id": "a6c5c795-f000-447e-9557-a1721eed6ec3",
        "number": 10
    }
]
```

##### Shorthand behavior

Given:
```javascript
[
    {
        "number": 2,
        "_id": "afba3ac8-2380-4634-9c65-f9b5318d6c82"
    }
]
```

Query:
```javascript
db.update({ number: 2 }, { $mult: 5 });
```

Result:
```javascript
[
    {
        "number": 10,
        "_id": "afba3ac8-2380-4634-9c65-f9b5318d6c82"
    }
]
```

#### $inc

Increments a property by a value.

Given:
```javascript
[
    {
        "number": 2,
        "_id": "0bcf572e-f044-4e09-a418-2c27306c086d"
    }
]
```

Query:
```javascript
db.update({ number: 2 }, { $inc: { number: 5 } });
```

Result:
```javascript
[
    {
        "_id": "0bcf572e-f044-4e09-a418-2c27306c086d",
        "number": 7
    }
]
```

##### Shorthand behavior

See $mult shorthand behavior - $inc behavior is no different.

#### $dec

Decrements a property by a value.

Given:
```javascript
[
    {
        "number": 2,
        "_id": "699fb2c4-89ab-4e4f-b3b5-a9512c02449d"
    }
]
```

Query:
```javascript
db.update({ number: 2 }, { $dec: { number: 5 } });
```

Result:
```javascript
[
    {
        "_id": "699fb2c4-89ab-4e4f-b3b5-a9512c02449d",
        "number": -3
    }
]
```

##### Shorthand behavior

See $mult shorthand behavior - $dec behavior is no different.

#### $div

Divides a property by a value.

Given:
```javascript
[
    {
        "number": 2,
        "_id": "f36fd0e1-3b7c-4217-8106-b235145d0d81"
    }
]
```

Query:
```javascript
db.update({ number: 2 }, { $div: { number: 2 } });
```

Result:
```javascript
[
    {
        "_id": "f36fd0e1-3b7c-4217-8106-b235145d0d81",
        "number": 1
    }
]
```

##### Shorthand behavior

See $mult shorthand behavior - $div behavior is no different.

### $pull

$pull is used to pull values out of a stored array.

Given:
```javascript
[
    {
        "nums": [
            1,
            2,
            3,
            4,
            5
        ],
        "_id": "8cc89828-b15e-475d-9532-856b8d6edf1b"
    }
]
```

Query:
```javascript
db.update({ nums: { $includesAll: [1] } }, { $pull: { nums: [2, 3, 4] } });
```

Result:
```javascript
[
    {
        "_id": "8cc89828-b15e-475d-9532-856b8d6edf1b",
        "nums": [
            1,
            5
        ]
    }
]
```

### $push

$push is used to push values into a stored array.

Given:
```javascript
[
    {
        "a": 1,
        "names": [
            {
                "first": "foo",
                "last": "bar"
            }
        ],
        "_id": "01ef1782-ba20-47fa-b02d-8f13f484f546"
    }
]
```

Query:
```javascript
db.update(
	{ a: 1 },
	{
		$push: {
			names: [
				{ first: "me", last: "em" },
				{ first: "hello", last: "world" },
			],
		},
	}
);

```

Result:
```javascript
[
    {
        "_id": "01ef1782-ba20-47fa-b02d-8f13f484f546",
        "a": 1,
        "names": [
            {
                "first": "foo",
                "last": "bar"
            },
            {
                "first": "me",
                "last": "em"
            },
            {
                "first": "hello",
                "last": "world"
            }
        ]
    }
]
```


### $unshift

$unshift is used to push values into the front of a stored array.

Given:
```javascript
[
    {
        "a": 1,
        "names": [
            {
                "first": "foo",
                "last": "bar"
            }
        ],
        "_id": "4d019887-d30f-409f-b253-870465eb2e73"
    }
]
```

Query:
```javascript
db.update({ a: 1 }, { $unshift: { names: { first: "hello", last: "world" } } });
```

Result:
```javascript
[
    {
        "_id": "4d019887-d30f-409f-b253-870465eb2e73",
        "a": 1,
        "names": [
            {
                "first": "hello",
                "last": "world"
            },
            {
                "first": "foo",
                "last": "bar"
            }
        ]
    }
]
```
### $insertBefore

Given an array of objects, use $insertBefore to insert an object before the object
that matches the query.

Given:
```javascript
[
    {
        "a": 1,
        "b": {
            "nums": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 4
                },
                {
                    "c": 5
                }
            ]
        },
        "_id": "a7f67995-781c-4f45-a153-8ff30d8bc509"
    }
]
```

Query:
```javascript
db.update({ c: 4 }, { $insertBefore: { c: 3 } });
```

Result:
```javascript
[
    {
        "_id": "a7f67995-781c-4f45-a153-8ff30d8bc509",
        "a": 1,
        "b": {
            "nums": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 3
                },
                {
                    "c": 4
                },
                {
                    "c": 5
                }
            ]
        }
    }
]
```

### $insertAfter

Given an array of objects, use $insertAfter to insert an object after the object
that matches the query.

Given:
```javascript
[
    {
        "a": 1,
        "b": {
            "nums": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 4
                },
                {
                    "c": 5
                }
            ]
        },
        "_id": "f0bed6b7-9642-4949-a5b4-f97fc3f6f639"
    }
]
```

Query:
```javascript
db.update({ c: 2 }, { $insertAfter: { c: 3 } });
```

Result:
```javascript
[
    {
        "_id": "f0bed6b7-9642-4949-a5b4-f97fc3f6f639",
        "a": 1,
        "b": {
            "nums": [
                {
                    "c": 1
                },
                {
                    "c": 2
                },
                {
                    "c": 3
                },
                {
                    "c": 4
                },
                {
                    "c": 5
                }
            ]
        }
    }
]
```
